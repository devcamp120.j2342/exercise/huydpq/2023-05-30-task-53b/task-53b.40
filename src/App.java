import models.Person;
import models.Staff;
import models.Student;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Nhi", "Bình tân");
        Person person2 = new Person("Kha", "Q10");
        
        System.out.println("Person 1: " );
        System.out.println(person1);
        System.out.println("Person 2: " );
        System.out.println(person2);
       

        Student student1 = new Student();
        Student student2 = new Student("Hoàng",person2.getName(), person2.getAddress(), 3, 10.0);
        System.out.println("Student 1: ");
        System.out.println( student1);
        System.out.println("Student 2: ");
        System.out.println(student2);

        Staff staff1 = new Staff(person1.getName(), person1.getAddress(), "FPT", 45000000);
        Staff staff2 = new Staff(person2.getName(), person2.getAddress(), "SG", 75000000);
        System.out.println("Staff 1: ");
        System.out.println(staff1);
        System.out.println("Staff 2: ");
        System.out.println(staff2);

    }
}
